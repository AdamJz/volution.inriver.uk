using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volution.Epicore.DTO;

namespace Volution.Epicore.CacheCompare
{
    public class Comparer
    {

        public static bool IsEqual (Article a, Article b)
        {
            var aDict = a.Properties;
            var bDict = b.Properties;

            var equals = (aDict.Count == bDict.Count) &&
                (!aDict.Values.Except(bDict.Values).Any()) &&
                (!bDict.Values.Except(aDict.Values).Any());

            return equals;
        }

        // Compare if item is removed from new file
        public static bool FlagOldValues (Article a, Article b)
        {
            var aDict = a.Properties;
            var bDict = b.Properties;

            var oldValues = bDict.Values.Except(aDict.Values).Any();

            return oldValues;
        }
    }
}
