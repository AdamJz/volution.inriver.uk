﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using inRiver.Remoting;
using inRiver.Remoting.Extension;
using inRiver.Remoting.Objects;
using Volution.Epicore.DTO;

namespace Volution.Epicore.inRiver
{
    public class EpicoreInboundData
    {
        public inRiverContext Context { get; set; }

        public IinRiverManager _Manager;

        public IinRiverManager Manager
        {
            get
            {
                if (_Manager == null)
                    _Manager = Context?.ExtensionManager;
                return _Manager;
            }
        }

        public Dictionary<string, string> GetMappings()
        {
            Context = Context ?? LocalContext.inRiverContext; // ADD THIS LOCAL 
            Dictionary<string, string> mappings = new Dictionary<string, string>();

            var defaultSettings = new DownloadData().DefaultSettings;
            var mappingRecords = (string.IsNullOrEmpty(Context?.Settings?["mappings"]) ? defaultSettings["mappings"] : Context.Settings["mappings"]).Split(':');

            foreach (string row in mappingRecords)
            {
                var kvp = row.Split(';'); // Splits between fields
                mappings.Add(kvp[0].Trim(), kvp[1].Trim());
            }

            return mappings;
        }

        // Method to parse the data to set the correct fieldtype
        private object ParseDataValue(Field field, object value)
        {
            try
            {
                object o;
                switch (field.FieldType.DataType.ToLower())
                {
                    case "localestring":
                        LocaleString ls = new LocaleString();
                        CultureInfo ci = new CultureInfo("en-GB");
                        ls.Languages.Add(ci);
                        ls[ci] = value.ToString();
                        o = ls;
                        break;
                    case "double":
                        CultureInfo ci2 = new CultureInfo("en-GB", true);
                        ci2.NumberFormat.DigitSubstitution = DigitShapes.NativeNational;
                        ci2.NumberFormat.NumberDecimalSeparator = ".";
                        Thread.CurrentThread.CurrentCulture = ci2;

                        var stringVal = value.ToString();
                        var doubleVal = string.IsNullOrEmpty(stringVal) ? 0 : Convert.ToDouble(stringVal, ci2);
                        o = doubleVal;
                        break;
                    case "cvl":
                        CVLValue cvl = Manager.ModelService.GetCVLValueByKey(value.ToString(), field.FieldType.CVLId);
                        if ((cvl == null) && (!string.IsNullOrEmpty(value.ToString())))
                            throw new Exception($"cvl key for keyId {value} in cvl {field.FieldType.CVLId} does not exist");
                        o = value;
                        break;
                    case "integer":
                        var strVal = value.ToString();
                        CultureInfo ci3 = new CultureInfo("en-GB", true);
                        ci3.NumberFormat.DigitSubstitution = DigitShapes.NativeNational;
                        ci3.NumberFormat.NumberDecimalSeparator = ".";
                        Thread.CurrentThread.CurrentCulture = ci3;
                        o = string.IsNullOrEmpty(strVal) ? 0 : Convert.ToDouble(strVal, ci3);
                        break;
                    case "boolean":
                        string[] vTrue = { "Y", "YES", "yes", "Yes", "True", "true", "TRUE" };
                        var boolVal = value.ToString();
                        o = vTrue.Contains(boolVal) ? true : false;
                        break;
                    case "datetime":
                        var date = value.ToString();
                        o = DateTime.ParseExact(date, "dd/MM/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                        break;
                    default:
                        o = value;
                        break;
                }
                return o;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        // Method to set the properties of the products
        private Entity SetProductProperties(Article a, Entity e, Dictionary<string, string> mappings)
        {
            var manager = Context.ExtensionManager;
            CultureInfo ci = new CultureInfo("en-GB");

            foreach (KeyValuePair<string, string> pair in a.Properties)
            {
                // Find matching inriver key for epicor key
                if (!mappings.ContainsKey(pair.Key))
                    continue;

                var inRiverKey = mappings[pair.Key];
                if (inRiverKey != null)
                {
                    var field = e.GetField(inRiverKey);

                    if (field != null)
                    {
                        field.Data = ParseDataValue(field, pair.Value);
                    }
                    
                    // HERE ONE CAN ADD SOME STATEMENT IFF THERE IS A FIELD THAT NEED TO BE SPLIT 
                }
            }
                       
            return e;
        }


        // Update inRiver with data from Epicor. If the article does not exist, we create it and then update inRiver
        public Dictionary<string, string> UpdateinRiverFromEpicorArticles(List<Article> articles)
        {
            Dictionary<string, string> errors = new Dictionary<string, string>(); // Send errors to inRiver if something went wrong
            var mappings = GetMappings();
            var manager = Context.ExtensionManager;

            foreach (Article a in articles)
            {
                try
                {
                    //Check if entity exists in inRiver
                    Entity entity = manager.DataService.GetEntityByUniqueValue("ProductArticleNumber", a.Id, LoadLevel.DataOnly); // String name in inRiver

                    if (entity == null)
                    {
                        entity = Entity.CreateEntity(manager.ModelService.GetEntityType(EntityTypes.PRODUCT));
                        entity = SetProductProperties(a, entity, mappings);
                        entity = manager.DataService.AddEntity(entity);
                    }
                    else
                    {
                        entity = manager.DataService.UpdateEntity(SetProductProperties(a, entity, mappings));
                    }
                }
                catch (Exception ex)
                {
                    errors.Add(a.Id, ex.Message);
                }
            }

            return errors;
        }

        // Method to set true/false to field in inRiver if it's removed in the new file from the FTP
        public Dictionary<string, string> UpdateInriverAboutOldValues(List<string> articles)
        {
            Dictionary<string, string> errors = new Dictionary<string, string>(); // Send errors to inRiver if something went wrong
            var mappings = GetMappings();
            var manager = Context.ExtensionManager;

            foreach (var a in articles)
            {
                try
                {
                    Entity entity = manager.DataService.GetEntityByUniqueValue("ProductArticleNumber", a, LoadLevel.DataOnly);

                    if (entity == null)
                    {
                        throw new System.InvalidOperationException("Entity can not be null"); ;
                    }

                    entity.GetField("ProductDeletedFileErp").Data = true;
                    entity = manager.DataService.UpdateEntity(entity);
                }
                catch (Exception ex)
                {
                    errors.Add(a, ex.Message);
                }
            }

            return errors;
        }
    }
}
