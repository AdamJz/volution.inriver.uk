﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using inRiver.Remoting;
using inRiver.Remoting.Extension;
using inRiver.Remoting.Log;
using inRiver.Remoting.Extension.Interface;
using FluentFTP;

using Volution.Epicore.DTO;
using Volution.Epicore.RedisCache;
using Volution.Epicore.CacheCompare;
using inRiver.Remoting.Objects;

namespace Volution.Epicore.inRiver
{
    public class DownloadData : IScheduledExtension
    {
        public inRiverContext Context { get; set; }

        public DownloadData() { }

        public Dictionary<string, string> DefaultSettings
        {
            get
            {
                var defaultSettings = new Dictionary<string, string>
                {
                    { "ftpServer", "92.42.73.175" }, //92.42.73.175/
                    { "ftpUser", "PIM" }, //PIM
                    { "ftpPassword", "PIM@vgn%" }, //PIM@vgn%
                    { "cacheKey", "VolutionAPICache.redis.cache.windows.net:6380,password = zBv0Kv3m4b8NwwOO5t7zoHFpaOpOTE9kIbh9xbVKXds= ,ssl = True,abortConnect = False"},
                    {"mappings", "Part_CheckBox02;ProductLoCarbon : Part_PartDescription;ProductName : Part_GrossWeight;ProductUnpackageWeight : Country_Description;ProductCoo : Part_MktParentChild_c;ProductChildParent : UDCodes_LongDesc;ProductStatus : UDCodes_ProductBrand_LongDesc;ProductBrand : Part_NetWeight;ProductPackedWeight : Part_PartHeight;ProductPackedHeight : Part_PartLength;ProductPackedDepth : Calculated_PartRev;ProductArticleNumber : Part_PartNum;ProductPartEpicor : Part_PartWidth;ProductPackedWidth : Calculated_Warranty;ProductWarranty : PartPC_ProdCode;ProductEan : PartRev2_RevisionNum;ProductArticleNumberRevision : PartPC_EAN14_ProdCode;ProductEanTwo : Calculated_SparePart;ProductSparepart : Calculated_ProductPackedVolume;ProductPackedVolume : PartXRefVend_VendPartNum;ProductSupplierArticleNumber : Vendor_Name;ProductSupplierName : Part_MktSpigotSize_c;ProductSpigotDiameter : UDCodes_ProductFunctions_LongDesc;ProductFunctions : Part_MktSELV_c;ProductSelv : UDCodes_ProductGrille_LongDesc;ProductGrille : UDCodes_Profile_LongDesc;ProductProfile : UDCodes_Operation_LongDesc;ProductModeContinuousIntermitent : UDCodes_InstType_LongDesc;ProductInstallationType : Part_MktDataLog_c;ProductDataLogging : UDCodes_Shutters_LongDesc;ProductShutters : Part_MktTotalSpeeds_c;ProductNumberOfSpeeds : Part_MktSpeedsInUse_c;ProductSpeedSettingsInUse : Part_MktLED_c;ProductLEDYesorNO : Part_MktMaxPerf_c;ProductFreeToAirM3H : Part_MktMaxW_c;New PIM Wattage fields linked to speed : UDCodes_IPX_LongDesc;ProductProtectionIP : Part_MktSoundMin_c;ProductMinSpeedSound : PartRev1_RevisionNum;ProductRevisionMostRecent : PartRev2_Approved;ProductRevisionActive : PartRev2_EffectiveDate;ProductRevisionEffectiveDate : Part_MktLiterature_c;ShowInLiterature : UDCodes_Channel_LongDesc;ProductMarketsegment" }
                    //"Calculated_PartRev; ProductArticleNumber : PartPC_ProdCode; ProductEan : Part_PartDescription; ProductName : Part_MktParentChild_c; ProductChildParent : UDCodes_CodeID; ProductStatus : ProductArticleNumberRevisionPartRev2_RevisionNum; ProductArticleNumberRevision : Calculated_SparePart; ProductSparepart : UDCodes_MarketSeg_CodeID; ProductMarketSegment : Part_CheckBox02; ProductLoCarbon : Part_GrossWeight; ProductUnpackageWeight: Part_NetWeight; ProductPackedWeight: Calculated_ProductPackedVolume; ProductPackedVolume: Part_PartWidth; ProductPackedWidth: Part_PartHeight; ProductPackedHeight: Part_PartLength; ProductPackedDepth: PartXRefVend_VendPartNum; ProductSupplierArticleNumber: Vendor_Name; ProductSupplierName: PartPC_EAN14_ProdCode; ProductEanTwo: Country_Description; ProductCoo: Part_PartNum; ProductPartEpicor: UDCodes_ProductBrand_CodeID; ProductBrand: Part_WarrantyCode; ProductWarranty: PartRev1_RevisionNum; ProductArticleNumberRevision" }
                    // : PartRev2_Approved; ProductRevisionActive : PartRev2_EffectiveDate; ProductRevisionEffectiveDate
           };
                return defaultSettings;
            }
        }

        public Dictionary<string, byte[]> ReadFileFromFTP()
        {
            Context = Context ?? LocalContext.inRiverContext; // HOPEFULLY THE LOCAL CONTEXT CAN BE REMOVED LATER

            //Change this when using FTP
            //byte[] files = File.ReadAllBytes(@"C:\Users\broderick.calpe\Downloads\Test.xlsx");
            //Dictionary<string, byte[]> fetchedData = new Dictionary<string, byte[]>();
            //fetchedData.Add("file", files);
            //return fetchedData;

            // THIS USED WHEN THE FTP HAVE FILES
            //string ftpServer = string.IsNullOrEmpty(Context.Settings?["ftpServer"]) ? DefaultSettings["ftpServer"] : Context.Settings["ftpServer"];
            //string ftpUser = string.IsNullOrEmpty(Context.Settings?["ftpUser"]) ? DefaultSettings["ftpUser"] : Context.Settings["ftpUser"];
            //string ftpPw = string.IsNullOrEmpty(Context.Settings?["ftpPassword"]) ? DefaultSettings["ftpPassword"] : Context.Settings["ftpPassword"];

            string ftpServer = DefaultSettings["ftpServer"];
            string ftpUser = DefaultSettings["ftpUser"];
            string ftpPw = DefaultSettings["ftpPassword"];

            try
            {
                 if (ftpServer == "")
                 {
                     return new Dictionary<string, byte[]>();
                 }

                 FtpClient client = new FtpClient(ftpServer);
                 client.Credentials = new NetworkCredential(ftpUser, ftpPw);
                 client.Connect();

                 //var ports = client.ActivePorts;  // Check ports in case you need to DEBUG
                 client.ConnectTimeout = 20000;

                 List<string> fileList = client.GetListing("/TestXML")
                     .Where(item => item.Type == FtpFileSystemObjectType.File)
                     .Select(file => 
                         file.FullName)
                     .ToList();

                 Context.Log(LogLevel.Information, "ReadFileFromFTP BEFORE");
                 Dictionary<string, byte[]> files = new Dictionary<string, byte[]>();

                 foreach (string fileName in fileList)
                 {
                     var stream = new MemoryStream();
                     client.Download(stream, fileName);

                     byte[] bytes = stream.GetBuffer();
                     files.Add(fileName, bytes);
                 }

                 client.Disconnect();

                 Context.Log(LogLevel.Information, "ReadFileFromFTP AFTER");
                return files;
            }
            catch (Exception ex)
            {
                Context.Log(LogLevel.Information, "ReadFileFromFTP Exception");
                return null;
            }


        }

        public async Task<List<Article>> CacheCompare(List<Article> articles)
        {
            List<Article> toInriver = new List<Article>();
            CacheDecorator cache = new CacheDecorator(string.IsNullOrEmpty(Context.Settings?["cacheKey"]) ? DefaultSettings["cacheKey"] : Context.Settings["cacheKey"]);

            foreach (Article a in articles)
            {
                // Check if article exists in cache
                var item = await cache.GetMyEntityAsync(a.Id);

                if (item != null)
                {
                    bool equal = Comparer.IsEqual(a, item);

                    if (!equal)
                    {
                        toInriver.Add(a);
                        await cache.UpdateEntityAsync(a);
                    }
                }
                else
                {
                    toInriver.Add(a);
                    await cache.UpdateEntityAsync(a);
                }
            }

            return toInriver;
        }

        public async Task<List<string>> CompareOldValues(List<Article> articles)
        {
            List<Article> toInriver = new List<Article>();
            CacheDecorator cache = new CacheDecorator(DefaultSettings["cacheKey"]);

            var allCacheKeys = await cache.FetchEntityAsync();

            var nonMatchingItems = allCacheKeys.Except(articles.Select(a => a.Id)).ToList();

            return nonMatchingItems;
        }


        public void Execute(bool force)
        {
            FileReader fileReader = new FileReader();

            //Context.Log(LogLevel.Information, "Volution Epicor Execute method called");

            EpicoreInboundData inData = new EpicoreInboundData();
            EpicoreInboundData oldData = new EpicoreInboundData(); // For values removed from new file

            Dictionary<string, byte[]> files = ReadFileFromFTP();

            foreach (KeyValuePair<string, byte[]> file in files)
            {
                try
                {
                    var articles = fileReader.Read(file);
                    List<Article> AsyncCache = AsyncHelper.RunSync(() => CacheCompare(articles));
                    inData.UpdateinRiverFromEpicorArticles(AsyncCache);

                    List<string> RemovedFromFile = AsyncHelper.RunSync(() => CompareOldValues(articles)); // Check if values are removed from file
                    oldData.UpdateInriverAboutOldValues(RemovedFromFile);
                }
                catch (Exception ex)
                {
                    Context.Log(LogLevel.Error, $"Something went wrong when importing {file.Key} : {ex.Message}");
                }
            }

            Context.Log(LogLevel.Information, "Executed Scheduled Extension");
        }

        public string Test()
        {
            try
            {
                var manager = new RemoteManager("https://remoting.productmarketingcloud.com");
                var ticket = manager.Authenticate("https://remoting.productmarketingcloud.com", "inriver.volution@istone.com", "Volution2018!", "test");
            }
            catch (Exception ex)
            {
                Context.Log(LogLevel.Error, $"Could not authenticate {ex.Message}");
            }
            return "Testing of IScheduledListener executed";
        }
    }
}
