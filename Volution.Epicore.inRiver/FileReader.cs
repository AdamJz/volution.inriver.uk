﻿using System;
using System.Collections.Generic;
using System.IO;
using Volution.Epicore.DTO;
using NPOI.XSSF.UserModel;
using System.Linq;
using System.Xml.Serialization;

namespace Volution.Epicore.inRiver
{
    public class FileReader
    {
        public FileReader() { }

        public List<Article> Read(KeyValuePair<string, byte[]> file)
        {
            List<Article> articles = new List<Article>();

            FileInfo fi = new FileInfo(file.Key);

            articles = fi.Extension.Equals(".xml") ? ReadXML(file.Value) : ReadExcel(file.Value);

            return articles;
        }

        public List<Article> ReadExcel(byte[] bytes)
        {
            List<Article> articles = new List<Article>();

            XSSFWorkbook xssfwb;
            XSSFSheet sheet;

            MemoryStream stream = new MemoryStream(bytes); // MAY NEED TO ADD CONVERT.TOBASE64STRING

            xssfwb = new XSSFWorkbook(stream);
            sheet = (XSSFSheet)xssfwb.GetSheetAt(0);

            var firstRow = sheet.GetRow(0);
            // Add statment to find what the column consists
            var match = new[] { "asd", "aasd", "dsad" }; //THIS IS PROBABLY NOT NEEDED AT ALL
            for (var i = 1; i < sheet.LastRowNum + 1; i++)
            {
                var article = new Article();
                var row = sheet.GetRow(i);
                for (var j = row.FirstCellNum; j < row.LastCellNum; j++)
                {
                    var value = row.GetCell(j) != null ? row.GetCell(j).ToString().Trim() : "";

                    // This whole if statement is only here if there are a column in which we need to split it up
                    if (match.Any(x => x.ToLower().Equals(firstRow.GetCell(j).ToString().ToLower())))
                    {
                        string[] delimiter = value.ToString().Split(','); // I DONT KNOW WHAT TO SPLIT ON YET, or if i really need to split
                        foreach (string d in delimiter)
                        {
                            var spl = d.Split(':'); // This split up the epicor and inriver values to find what to add to them
                            if (spl.Count() > 1)
                                article.Properties.Add(spl[0].Trim(), spl[1].Trim());
                        }
                    }
                    else
                    {
                        article.Properties.Add(firstRow.GetCell(j).ToString(), value); // THIS IS THE IMPORTANT THING; IT READS ALL THE ROWS
                    }
                }
                article.UniqueHash = article.MD5Hash();
                articles.Add(article);
            }

            return articles;
        }

        public List<Article> ReadXML(byte[] bytes)
        {
            var articles = new List<Article>();

            QueryResultDataSet productProperties = new QueryResultDataSet();
            MemoryStream memstream = new MemoryStream(bytes);
            XmlSerializer serializer = new XmlSerializer(typeof(QueryResultDataSet));
            productProperties = (QueryResultDataSet)serializer.Deserialize(memstream);

            var properties = productProperties.Items.ToList();

            foreach (var property in properties)
            {
                var article = new Article();
                var type = property.GetType();
                foreach (var prop in type.GetProperties())
                {
                    var value = prop.GetValue(property) != null ? prop.GetValue(property) : "";
                    var propName = prop.ToString();
                    article.Properties.Add(prop.Name, value.ToString());
                }

                article.UniqueHash = article.MD5Hash();
                articles.Add(article);
            }

            return articles;
        }
    }
}

