﻿using inRiver.Remoting;
using inRiver.Remoting.Extension;

namespace Volution.Epicore.inRiver
{
    internal static class LocalContext
    {
        private static inRiverContext _Context;

        internal static inRiverContext inRiverContext
        {
            get
            {
                var defaultSettings = new DownloadData().DefaultSettings;

                if (_Context == null)
                    _Context = new inRiverContext(RemoteManager.CreateInstance("https://remoting.productmarketingcloud.com", "inriver.volution@istone.se", "Volution2018!", "test"), new ConsoleLogger());
                return _Context;
            }
            set { }
        }
    }
}
