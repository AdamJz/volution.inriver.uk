﻿namespace Volution.Epicore.inRiver
{
    class EntityTypes
    {
        public const string FAMILY = "Family";
        public const string PRODUCT = "Product";
        public const string RESOURCE = "Resource";
    }
}
