﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using inRiver.Remoting;
using inRiver.Remoting.Extension;
using inRiver.Remoting.Extension.Interface;
using inRiver.Remoting.Log;
using inRiver.Remoting.Objects;
using Volution.Epicore.DTO;
using Volution.Epicore.RedisCache;

namespace Volution.Epicore.inRiver
{
    class EntityListener : IEntityListener
    {
        public inRiverContext Context { get; set; }

        public Dictionary<string, string> DefaultSettings
        {
            get
            {
                DownloadData data = new DownloadData();
                var defaultSettings = data.DefaultSettings;
                return defaultSettings;
            }
        }

        // NEED TO FIND WHAT TO TRIGGER THIS ON FROM INRIVER.  IS IT NOT a.Id?? I HAVE ENTITY IN
        // ADD THAT TO THE variable product
        public void EntityDeleted(Entity entity)
        {
            var product = Context.ExtensionManager.DataService.GetEntityByUniqueValue("ProductArticleNumber", "", LoadLevel.DataOnly);
            CacheDecorator cache = new CacheDecorator(string.IsNullOrEmpty(Context.Settings?["cacheKey"]) ? DefaultSettings["cacheKey"] : Context.Settings["cacheKey"]);

            Article a = new Article();
            var id = product.Id;
            var key = $"MyEntity:{id}";

            if (product == null)
            {
                Console.WriteLine($"A product with product number {product.Id} does not exist");
                return;
            }

            bool deleteOk = Context.ExtensionManager.DataService.DeleteEntity(product.Id);
            if (deleteOk)
            {
                var deletedItem = cache.DeleteEntityAsync(key, a);
                Console.WriteLine($"Successfully deleted product with system id {product.Id}");
            }
            else
            {
                Console.WriteLine($"Failed to delete product with system id {product.Id}");
            }

        }

        public string Test()
        {
            try
            {
                var manager = new RemoteManager("https://remoting.productmarketingcloud.com");
                var ticket = manager.Authenticate("https://remoting.productmarketingcloud.com", "inriver.volution@istone.com", "Volution2018!", "test");
            }
            catch (Exception ex)
            {
                Context.Log(LogLevel.Error, $"Could not authenticate {ex.Message}");
            }
            return "Testing of IEntityListener executed";
        }

        public void EntityCommentAdded(int entityId, int commentId) { }
        public void EntityCreated(int entityId) { }
        public void EntityFieldSetUpdated(int entityId, string fieldSetId) { }
        public void EntityLocked(int entityId) { }
        public void EntitySpecificationFieldAdded(int entityId, string fieldName) { }
        public void EntitySpecificationFieldUpdated(int entityId, string fieldName) { }
        public void EntityUnlocked(int entityId) { }
        public void EntityUpdated(int entityId, string[] fields) { }
    }
}
