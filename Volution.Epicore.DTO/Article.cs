﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

namespace Volution.Epicore.DTO
{
    public class Article
    {
        public string Id { get { return Properties["Calculated_PartRev"]; } } // I THINK THIS IS THE CONCATENATED FIELD OF ARTNO AND REVNO FROM EPICOR
        public string UniqueHash { get; set; }

        private Dictionary<string, string> _Properties = null;
        public Dictionary<string, string> Properties
        {
            get
            {
                if (_Properties == null)
                    _Properties = new Dictionary<string, string>();
                return _Properties;
            }
        }
    }

    public static class ArticleHash
    {
        public static string MD5Hash (this Article a)
        {
            string input = JsonConvert.SerializeObject(a.Properties.OrderByDescending(x => x.Key));
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for(int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
    }
}
