using System;
using System.Threading.Tasks;
using StackExchange.Redis;
using Newtonsoft.Json;
using inRiver.Remoting.Extension;
using Volution.Epicore.DTO;
using System.Collections.Generic;

namespace Volution.Epicore.RedisCache
{
    public class CacheDecorator
    {
        public static ConnectionMultiplexer Connection => lazyConnection.Value;
        public static IDatabase RedisCache => Connection.GetDatabase();
        private static string _cacheKey;
        public inRiverContext Context { get; set; }

        private static Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            return ConnectionMultiplexer.Connect(_cacheKey);
        });

        public CacheDecorator (string cacheKey)
        {
            _cacheKey = cacheKey;
            IDatabase cache = lazyConnection.Value.GetDatabase();
        }

        private string GetItemKey (string id)
        {
            return $"MyEntity:{id}";
        }

        private string GetItemId(string key)
        {
            return key.TrimStart("MyEntity:".ToCharArray());
        }

        // Method to retrieve object from the cache
        public async Task<Article> GetMyEntityAsync(string id)
        {
            var json = await RedisCache.StringGetAsync(GetItemKey(id)).ConfigureAwait(false);
            var itemValue = string.IsNullOrWhiteSpace(json)
                ? default(Article)
                : JsonConvert.DeserializeObject<Article>(json);

            return itemValue;
        }

        // Method to update the cache
        public async Task UpdateEntityAsync(Article a)
        {
            await RedisCache.StringSetAsync(GetItemKey(a.Id), JsonConvert.SerializeObject(a)).ConfigureAwait(false);
        }

        // Method to delete product from cache iff deleted in inRiver
        public async Task DeleteEntityAsync(string id, Article a)
        {
            await RedisCache.SetRemoveAsync(GetItemKey(id), JsonConvert.SerializeObject(a)).ConfigureAwait(false);
        }

        // Method to retrieve the full list of the database with the keys for Epicor to use
        // for comparing new file to old file and set a value to the field ProductDeletedFileErp
    
        public async Task<List<string>> FetchEntityAsync()
        {
            List<string> listOfKeys = new List<string>();
            var server = Connection.GetServer("VolutionAPICache.redis.cache.windows.net:6380");
            foreach(var key in server.Keys())
            {
               listOfKeys.Add(GetItemId(key));
            }
            return listOfKeys;
        }
    }
}
