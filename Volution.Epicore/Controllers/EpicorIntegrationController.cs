﻿using System.Threading.Tasks;
using System.Web.Http;
using Volution.Epicore.inRiver;

namespace Volution.Epicore.Controllers
{
    [RoutePrefix("epicore")]
    public class EpicorIntegrationController : ApiController
    {
        [Route("trigger")]
        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            var downloader = new DownloadData();
            downloader.Execute(true);

            return Ok();
        }
    }
}
